import React, { useState } from "react"
import { Image, View, Alert } from "react-native"
import { Body, Item, Input, Button, Card, CardItem, Col, Container, Content, Footer, FooterTab, Grid, H3, Header, Icon, Left, List, ListItem, Right, Text, Thumbnail, Title } from "native-base"
import { ScrollView } from "react-native-gesture-handler"
import styles from "./styles"
import { useSelector, useDispatch } from "react-redux"
import { put } from "../../helpers/fetch"
import { root_setUser } from "../../stores/root.action"

const wahyu = require("../../assets/img/author.jpg")

const MyProfileKeamanan = ({ navigation }) => {
	const { user } = useSelector(state => state.root)
	const dispatch = useDispatch()

	const [isEdit, setIsEdit] = useState(false)
	const [visible, setVisible] = useState({
		myPassword: false,
		newPassword: false,
		newPasswordConfirm: false,
	})
	const [form, setForm] = useState({
		newPassword: "",
		newPasswordConfirm: "",
	})

	const handleForm = (name, value) => setForm({ ...form, [name]: value })

	const handleUpdatePassword = () => {
		if (form.newPassword === form.newPasswordConfirm) {
			put(`updatepassword/${user.id}`, { password: form.newPassword }).then(res => {
				if (res.status === "success") {
					Alert.alert("Berhasil", "Password berhasil diubah.")
					dispatch(root_setUser({ password: form.newPassword }))
				} else {
					Alert.alert("Gagal", "Password gagal diubah, coba lagi.")
				}
			})
			.catch(err => Alert.alert("Error", JSON.stringify(err)))
		} else {
			Alert.alert("Password tidak sama", "Password baru dengan konfirmasi berbeda")
		}
	}

	return (
		<Container style={styles.container}>
			<Header transparent androidStatusBarColor="#fff">
				<Left>
					<Image style={{ height: 30 }} source={require("../../assets/img/logo-covid-header.png")} />
				</Left>
				<Body>
					<Title style={{ color: "#4F4F4F", marginLeft: 52 }}>My Profile</Title>
				</Body>
				<Right />
			</Header>

			<ScrollView>
				<Content padder>

					<Card>
						<View style={{ justifyContent: "center", alignItems: "center", backgroundColor: "#0486FE" }}>
							<Thumbnail large style={{ marginTop: 16 }} source={wahyu} />
							<Text style={{ color: "white", fontSize: 18, marginTop: 16 }}>{user.fullname}</Text>
						</View>

						<CardItem style={{ backgroundColor: "#0486FE" }}>
							<Grid style={{ marginBottom: 16 }}>
								<Col>
									<Button 
										block 
										onPress={() => navigation.navigate("MyProfile")}
										style={{
										backgroundColor: "#294F74",
										marginRight: 8,
										shadowColor: 'rgba(0, 0, 0, 0.1)',
										shadowOpacity: 0.8,
										elevation: 6,
										shadowRadius: 15,
										shadowOffset: { width: 1, height: 13 }
									}}>
										<Text>Data Personal</Text>
									</Button>
								</Col>
								<Col>
									<Button
										block
										style={{
											backgroundColor: "#0486FE",
											marginLeft: 8,
											shadowColor: 'rgba(0, 0, 0, 0.1)',
											shadowOpacity: 0.8,
											elevation: 6,
											shadowRadius: 15,
											shadowOffset: { width: 1, height: 13 }
										}}>
										<Text>Keamanan</Text>
									</Button>
								</Col>
							</Grid>
						</CardItem>
					</Card>

					<List>
						<Card>
							<CardItem header>
								<H3 style={{ color: "#FEAC0E" }}>Keamanan Password</H3>
							</CardItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>Username</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon style={{ color: "#FEAC0E" }} name='screen-smartphone' type="SimpleLineIcons" />
											<Input value={user.username} disabled placeholder="Username" />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>Password</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon style={{ color: "#FEAC0E" }} name='lock' type="SimpleLineIcons" />
											<Input value={user.password} disabled secureTextEntry={!visible.myPassword} placeholder="Username" />
											<Icon onPress={() => setVisible({ ...visible, myPassword: !visible.myPassword })} active name='eye' type="SimpleLineIcons" style={{ marginRight: 8 }} />
										</Item>
									</Body>
								</Left>
							</ListItem>

							{
								isEdit ?
								<>
								<ListItem>
									<Left>
										<Body style={{ marginLeft: 0 }}>
											<Text note>Password baru</Text>
											<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
												<Icon style={{ color: "#FEAC0E" }} name='lock' type="SimpleLineIcons" />
												<Input value={form.newPassword} onChangeText={v => handleForm("newPassword", v)} secureTextEntry={!visible.newPassword} placeholder="Input password baru anda" />
												<Icon onPress={() => setVisible({ ...visible, newPassword: !visible.newPassword })} active name='eye' type="SimpleLineIcons" style={{ marginRight: 8 }} />
											</Item>
										</Body>
									</Left>
								</ListItem>

								<ListItem>
									<Left>
										<Body style={{ marginLeft: 0 }}>
											<Text note>Konfirmasi Password</Text>
											<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
												<Icon active style={{ color: "#FEAC0E" }} name='lock' type="SimpleLineIcons" />
												<Input value={form.newPasswordConfirm} onChangeText={v => handleForm("newPasswordConfirm", v)} secureTextEntry={!visible.newPasswordConfirm} placeholder="Konfirmasi password" />
												<Icon onPress={() => setVisible({ ...visible, newPasswordConfirm: !visible.newPasswordConfirm })} active name='eye' type="SimpleLineIcons" style={{ marginRight: 8 }} />
											</Item>
										</Body>
									</Left>
								</ListItem>
								<View style={{ paddingLeft: 16, paddingRight: 16, paddingBottom: 16 }}>
									<Button
										block
										onPress={handleUpdatePassword}
										style={{ backgroundColor: "#0677F9", marginTop: 16 }}>
										<Text>SIMPAN</Text>
									</Button>
									<Button
										block
										onPress={() => setIsEdit(false)}
										style={{ backgroundColor: "white", borderWidth: 1, borderColor: "#000", marginTop: 16 }}>
										<Text style={{ color: "#000" }}>BATAL</Text>
									</Button>
								</View>
								</>
								:
								<>
								<View style={{ paddingLeft: 16, paddingRight: 16, paddingBottom: 16 }}>
									<Button
										block
										onPress={() => setIsEdit(true)}
										style={{ backgroundColor: "#0677F9", marginTop: 16 }}>
										<Text>EDIT</Text>
									</Button>
								</View>
								</>
							}
						</Card>
					</List>


				</Content>
			</ScrollView>

			<Footer>
				<FooterTab style={{ backgroundColor: "white" }}>
					<Button
						vertical
						style={{ backgroundColor: "white" }}
						onPress={() => navigation.navigate("Dashboard")}>
						<Icon style={{ color: "grey" }} type="AntDesign" name="home" />
						<Text style={{ color: "grey", fontSize: 9 }}>Home</Text>
					</Button>
					<Button vertical style={{ backgroundColor: "white" }} >
						<Icon style={{ color: "grey" }} type="AntDesign" name="notification" />
						<Text style={{ fontSize: 9 }}>Reminder</Text>
					</Button>
					<Button
						vertical
						style={{ backgroundColor: "white" }}
						onPress={() => navigation.navigate("ScreeningListView")}>
						<Icon style={{ color: "grey" }} type="AntDesign" name="solution1" />
						<Text style={{ fontSize: 9 }}>Screening</Text>
					</Button>
					<Button
						vertical
						style={{ backgroundColor: "white" }}
						onPress={() => navigation.navigate("MyProfile")}>
						<Icon style={{ color: "#FEAC0E" }} type="AntDesign" name="user" />
						<Text style={{ fontSize: 9, color: "#FEAC0E" }}>Profile</Text>
					</Button>
				</FooterTab>
			</Footer>
		</Container>
	)
}

export default MyProfileKeamanan
