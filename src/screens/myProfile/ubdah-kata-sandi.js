import { Body, Button, Card, Container, Content, Footer, FooterTab, H3, Header, Icon, Input, Item, Left, List, ListItem, Right, Text, Title } from "native-base";
import React, { Component } from "react";
import { ScrollView } from "react-native-gesture-handler";
import styles from "./styles";


class UbahKataSandi extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined
    };
  }
  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header transparent androidStatusBarColor="#fff">
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate("MyProfile")}>
              <Icon style={{color:"#4F4F4F"}} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={{color: "#4F4F4F"}}>Ubah Kata Sandi</Title>
          </Body>
          <Right/>
        </Header>

        <ScrollView style={{marginTop: 8}} >
          <Content padder>

            <H3 style={{color: "#6AC101", fontWeight: "normal", marginBottom: 16}}>Masukkan Kata Sandi Baru Anda</H3>

            <List>
              <Card>
                <ListItem>
                  <Left>
                    <Body>
                      <Text note style={{color: "#6AC101"}}>New Password</Text>
                      <Item rounded style={{marginTop: 8, backgroundColor: "white"}}>
                        <Icon active name='lock' type="SimpleLineIcons" />
                        <Input placeholder='Please fill new password' />
                      </Item>
                    </Body>
                  </Left>
                </ListItem>
                <ListItem>
                  <Left>
                    <Body>
                      <Text note style={{color: "#6AC101"}}>Confirm New Password</Text>
                      <Item rounded style={{marginTop: 8, backgroundColor: "white"}}>
                        <Icon active name='lock' type="SimpleLineIcons" />
                        <Input placeholder='Please confirm password' />
                      </Item>
                    </Body>
                  </Left>
                </ListItem>
              </Card>
            </List>

            <Button block rounded style={{backgroundColor: "#0677F9", marginTop: 8}}><Text>Save</Text></Button>

          </Content>
        </ScrollView>
      </Container>
    );
  }
}

export default UbahKataSandi;
