import {
  Body,
  Card,
  CardItem,
  Icon,
  Input,
  Item,
  Left,
  ListItem,
  Text,
  Grid,
  Col,
  Button,
} from "native-base"
import React from "react"

const ScreenProsesKesehatan = (props) => {

  console.log("coviD", props)
  if (props.hide) {
    return null
  }

  let status = ""
  if(props.status == 1){
    status = "OTG"
  } else if (props.status == 2){
    status = "ODP* - Ringan"
  } else {
    status = "ODP* - Berat"
  }

  return (
    <Card>
      <CardItem header>
        <Text style={{color: "#FEAC0E"}}>Status Kesehatan</Text>
      </CardItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Grid style={{marginBottom: 16}}>
              <Col style={{flex: 4}}>
                <Text note style={{marginBottom: 8}}>
                  Demam
                </Text>
                <Item regular style={{backgroundColor: "white"}}>
                  <Input disabled={true} placeholder="Demam" />
                </Item>
              </Col>
              <Col style={{flex: 2}}>
                <Text note style={{marginBottom: 8}}>
                  Suhu
                </Text>
                <Item regular style={{backgroundColor: "white"}}>
                  <Input disabled={true} placeholder="Suhu" />
                </Item>
              </Col>
              <Col style={{flex: 2}}>
                <Text style={{marginBottom: 8}}></Text>
                <Button
                  block
                  style={{marginLeft: 8, backgroundColor: "#05C07C"}}>
                  <Text>YA</Text>
                </Button>
              </Col>
              <Col style={{flex: 3}}>
                <Text style={{marginBottom: 8}}></Text>
                <Button
                  danger
                  block
                  style={{marginLeft: 8, backgroundColor: "#DA132F"}}>
                  <Text>TIDAK</Text>
                </Button>
              </Col>
            </Grid>
          </Body>
        </Left>
      </ListItem>
      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Grid style={{marginBottom: 16}}>
              <Col style={{flex: 6}}>
                <Text note style={{marginBottom: 8}}>
                  Batuk Kering
                </Text>
                <Item regular style={{backgroundColor: "white"}}>
                  <Input disabled={true} placeholder="Batuk Kering" />
                </Item>
              </Col>
              <Col style={{flex: 2}}>
                <Text style={{marginBottom: 8}}></Text>
                <Button
                  block
                  style={{marginLeft: 8, backgroundColor: "#05C07C"}}>
                  <Text>YA</Text>
                </Button>
              </Col>
              <Col style={{flex: 3}}>
                <Text style={{marginBottom: 8}}></Text>
                <Button
                  danger
                  block
                  style={{marginLeft: 8, backgroundColor: "#DA132F"}}>
                  <Text>TIDAK</Text>
                </Button>
              </Col>
            </Grid>
          </Body>
        </Left>
      </ListItem>
      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Grid style={{marginBottom: 16}}>
              <Col style={{flex: 6}}>
                <Text note style={{marginBottom: 8}}>
                  Sakit Tenggorokan
                </Text>
                <Item regular style={{backgroundColor: "white"}}>
                  <Input disabled={true} placeholder="Sakit Tenggorokan" />
                </Item>
              </Col>
              <Col style={{flex: 2}}>
                <Text style={{marginBottom: 8}}></Text>
                <Button
                  block
                  style={{marginLeft: 8, backgroundColor: "#05C07C"}}>
                  <Text>YA</Text>
                </Button>
              </Col>
              <Col style={{flex: 3}}>
                <Text style={{marginBottom: 8}}></Text>
                <Button
                  danger
                  block
                  style={{marginLeft: 8, backgroundColor: "#DA132F"}}>
                  <Text>TIDAK</Text>
                </Button>
              </Col>
            </Grid>
          </Body>
        </Left>
      </ListItem>
      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Grid style={{marginBottom: 16}}>
              <Col style={{flex: 6}}>
                <Text note style={{marginBottom: 8}}>
                  Sakit Kepala
                </Text>
                <Item regular style={{backgroundColor: "white"}}>
                  <Input disabled={true} placeholder="Sakit Kepala" />
                </Item>
              </Col>
              <Col style={{flex: 2}}>
                <Text style={{marginBottom: 8}}></Text>
                <Button
                  block
                  style={{marginLeft: 8, backgroundColor: "#05C07C"}}>
                  <Text>YA</Text>
                </Button>
              </Col>
              <Col style={{flex: 3}}>
                <Text style={{marginBottom: 8}}></Text>
                <Button
                  danger
                  block
                  style={{marginLeft: 8, backgroundColor: "#DA132F"}}>
                  <Text>TIDAK</Text>
                </Button>
              </Col>
            </Grid>
          </Body>
        </Left>
      </ListItem>
      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Grid style={{marginBottom: 16}}>
              <Col style={{flex: 6}}>
                <Text note style={{marginBottom: 8}}>
                  Lemas
                </Text>
                <Item regular style={{backgroundColor: "white"}}>
                  <Input disabled={true} placeholder="Lemas" />
                </Item>
              </Col>
              <Col style={{flex: 2}}>
                <Text style={{marginBottom: 8}}></Text>
                <Button
                  block
                  style={{marginLeft: 8, backgroundColor: "#05C07C"}}>
                  <Text>YA</Text>
                </Button>
              </Col>
              <Col style={{flex: 3}}>
                <Text style={{marginBottom: 8}}></Text>
                <Button
                  danger
                  block
                  style={{marginLeft: 8, backgroundColor: "#DA132F"}}>
                  <Text>TIDAK</Text>
                </Button>
              </Col>
            </Grid>
          </Body>
        </Left>
      </ListItem>
      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Grid style={{marginBottom: 16}}>
              <Col style={{flex: 6}}>
                <Text note style={{marginBottom: 8}}>
                  Sesak Napas
                </Text>
                <Item regular style={{backgroundColor: "white"}}>
                  <Input disabled={true} placeholder="Sesak Napas" />
                </Item>
              </Col>
              <Col style={{flex: 2}}>
                <Text style={{marginBottom: 8}}></Text>
                <Button
                  block
                  style={{marginLeft: 8, backgroundColor: "#05C07C"}}>
                  <Text>YA</Text>
                </Button>
              </Col>
              <Col style={{flex: 3}}>
                <Text style={{marginBottom: 8}}></Text>
                <Button
                  danger
                  block
                  style={{marginLeft: 8, backgroundColor: "#DA132F"}}>
                  <Text>TIDAK</Text>
                </Button>
              </Col>
            </Grid>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Status Kondisi</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Input placeholder={status} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>
    </Card>
  )
}

export default ScreenProsesKesehatan
