import {
    Body,
    Card,
    CardItem,
    Input,
    Item,
    Left,
    ListItem,
    Text,
  } from "native-base"
  import React from "react"
  
  const ScreenProsesInfoLain = (props) => {
    if (props.hide) {
      return null
    }
  
    return (
      <Card>
        <CardItem header>
          <Text style={{color: "#FEAC0E"}}>Informasi Lain</Text>
        </CardItem>
  
        <ListItem>
          <Left>
            <Body style={{marginLeft: 0}}>
              <Text note>Apakah di area tempat tinggal ada yang terkena COVID 19?</Text>
              <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
                <Input disabled={true} placeholder="Ya" />
              </Item>
            </Body>
          </Left>
        </ListItem>

        <ListItem>
          <Left>
            <Body style={{marginLeft: 0}}>
              <Text note>Catatan lain</Text>
              <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
                <Input disabled={true} placeholder="Catatan" />
              </Item>
            </Body>
          </Left>
        </ListItem>
      </Card>
    )
  }
  
  export default ScreenProsesInfoLain
  