import {
  Body,
  Card,
  CardItem,
  Icon,
  Input,
  Item,
  Left,
  ListItem,
  Text,
  Grid,
  Col,
  Button,
} from "native-base"
import React from "react"
import { useNavigation } from '@react-navigation/native';

const ScreenProsesRujukan = (props) => {
    const { navigate } = useNavigation();
  if (props.hide) {
    return null
  }

  return (
    <Card>
      <CardItem header>
        <Text style={{color: "#FEAC0E"}}>Rujukan Pasien Ke</Text>
      </CardItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Grid style={{marginBottom: 16}}>
              <Col style={{flex: 4}}>
                <Col style={{flex: 2}}>
                  <Text note style={{marginBottom: 8}}>
                    Faskes
                  </Text>
                  <Item
                    regular
                    style={{backgroundColor: "white", marginLeft: 0}}>
                    <Input disabled={true} placeholder="Faskes" />
                  </Item>
                </Col>
              </Col>
              <Col style={{flex: 3}}>
                <Text style={{marginBottom: 8}}></Text>
                <Button
                  danger
                  block
                  onPress={() => navigate("ProcessScreeningSeven")}
                  style={{marginLeft: 8, backgroundColor: "#007aff"}}>
                  <Text>INFO FASKES</Text>
                </Button>
              </Col>
            </Grid>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Hari dan Tanggal</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="calendar"
                type="SimpleLineIcons"
              />
              <Input disabled={true} placeholder="Date" />
            </Item>
          </Body>
        </Left>
      </ListItem>
    </Card>
  )
}

export default ScreenProsesRujukan
