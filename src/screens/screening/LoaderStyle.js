import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    opacity: 0.5,
    width: '100%',
    height: '100%',
    // alignSelf: 'center',
    // flexDirection: 'column',
  },
});

export default styles;
