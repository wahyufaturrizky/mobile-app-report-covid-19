import React, { useState, useEffect } from "react"
import { View, Alert } from "react-native"
import { Body, Switch, DatePicker, Badge, Form, Picker, Grid, Col, Button, Card, CardItem, Container, Content, H3, Header, Icon, Input, Item, Left, List, ListItem, Right, Text } from "native-base"
import { ScrollView } from "react-native-gesture-handler"
import { useSelector, useDispatch } from "react-redux"
import moment from "moment"
import styles from "./styles"

import { post } from "../../helpers/fetch"
import ucfirst from "../../helpers/ucfirst"
import { root_setKabupaten, root_setKecamatan, root_setKelurahan } from "../../stores/root.action"
import { screening_setScreeningId, screening_getDataKtp, screening_handleFormKtp, screening_reset } from "../../stores/screening.action"


const ProcessScreeningOne = ({ navigation }) => {
	const { root: { negara, provinsi, kabupaten, kecamatan, kelurahan }, screening: { screeningId, formktp } } = useSelector(state => state)
	const dispatch = useDispatch()
	
	const [searchNoKtp, setSearchNoKtp] = useState("")

	const today = moment(new Date()).format("DD MMMM YYYY")

	const handleForm = (name, value) => dispatch(screening_handleFormKtp(name, value))
	const handleDate = (name, value) => dispatch(screening_handleFormKtp(name, moment(value).format("YYYY-MM-DD")))
	const handleSearchKtp = async () => {
		if (searchNoKtp !== "") {
			await dispatch(screening_getDataKtp(searchNoKtp))
		} else {
			Alert.alert("No. KTP Kosong", "Masukkan No. KTP dahulu untuk mencari.")
		}
	}
	
	const handleChangeProvinsi = (name, id) => {
		handleForm(name, id)
		post(`http://ems-service.jatimprov.smartcovid19.com/api/get-kab`, { id }).then(resKabupaten => {
			if (resKabupaten.success) {
				dispatch(root_setKabupaten(resKabupaten.data))
			}
		})
	}

	const handleChangeKabupaten = (name, id) => {
		handleForm(name, id)
		post(`http://ems-service.jatimprov.smartcovid19.com/api/get-kec`, { id }).then(resKecamatan => {
			if (resKecamatan.success) {
				dispatch(root_setKecamatan(resKecamatan.data))
			}
		})
	}

	const handleChangeKecamatan = (name, id) => {
		handleForm(name, id)
		post(`http://ems-service.jatimprov.smartcovid19.com/api/get-kel`, { id }).then(resKelurahan => {
			if (resKelurahan.success) {
				dispatch(root_setKelurahan(resKelurahan.data))
			}
		})
	}

	const handleEnterNoHp = e => {			
		if(e.nativeEvent.key === " " && formktp.isWa){
			Alert.alert("Konfirmasi", "Apakah No. Whatsapp (WA) anda sama dengan No. Telepon atau voice contact ?", [
				{text: "Sama", onPress: () => {
					handleForm("telp_no", formktp.wa_no)
				}},
				{text: "Berbeda", onPress: () => {}},
			])
		}
	}

	const handleFastScreening = () => {
		Alert.alert("Pemberitahuan", "Anda akan melakukan screening cepat\n\nSilahkan mengisi kontak Warga screening.")
		navigation.navigate("ProcessScreeningCepat")
	}
	const handleReset = () => dispatch(screening_reset())
	const handleNext = () => {
		const { isWa, isTelp, wa_no, telp_no } = formktp
		if ((isWa && wa_no !== "") ? true : (isTelp && telp_no !== "")) {
			navigation.navigate("ProcessScreeningTwo")
		} else {
			if (isWa && wa_no === "") {
				Alert.alert("No. Whatsapp Kosong", "Silahkan lengkapi No. WA sebelum melanjutkan.")
			} else if (!isWa && isTelp && telp_no === "") {
				Alert.alert("No. Telepon belum lengkap", "Silahkan lengkapi No. Telepon Pribadi (Karena tanpa No. WA) sebelum melanjutkan.")				
			} else if (!isWa && !isTelp && wa_no === "" && telp_no === "") {
				Alert.alert("No. Kontak belum lengkap", "Silahkan lengkapi No. WA / No. Telepon Pribadi sebelum melanjutkan.")
			} else {
				Alert.alert("Identitas belum lengkap", "Silahkan lengkapi data identitas sebelum melanjutkan.")
			}
		}
	}

	const handleCheckWa = () => {
		if (formktp.isWa) {
			dispatch(screening_handleFormKtp("wa_no", ""))
		}
		dispatch(screening_handleFormKtp("isWa", !formktp.isWa))
	}
	
	const handleCheckTelp = () => {
		if (formktp.isTelp) {
			dispatch(screening_handleFormKtp("telp_no", ""))
		}
		dispatch(screening_handleFormKtp("isTelp", !formktp.isTelp))
	}


	useEffect(() => {
		if (!screeningId || screeningId === null) {
			dispatch(screening_setScreeningId())
		}
	}, [dispatch])

	return (
		<Container style={styles.container}>
			<Header transparent androidStatusBarColor="#fff">
				<Left>
					<Button transparent onPress={() => navigation.navigate("Dashboard")}>
						<Icon style={{ color: "#4F4F4F" }} name="md-arrow-back" />
					</Button>
				</Left>
				<Body>
					<Text style={{ color: "#4F4F4F" }}>Process Screening</Text>
				</Body>
				<Right />
			</Header>

			<ScrollView style={{ marginTop: 8 }} >
				<Content padder>

					<List style={{ marginBottom: 16 }}>
						<Card>
							<ListItem onPress={() => navigation.navigate("ScreeningProcessScreeningTwo")}
								style={{ marginLeft: 4 }}>

								<Body>
									<View style={{ justifyContent: "space-between", flexDirection: "row" }}>
										<Text>ID SCREENING</Text>
										<Badge style={{ backgroundColor: "#04B974", marginRight: 8 }}>
											<Text>{today}</Text>
										</Badge>
									</View>
									<Text style={{ color: "#F2994A", marginTop: 0 }} note numberOfLines={1}>{screeningId}</Text>
								</Body>
							</ListItem>
						</Card>
					</List>

					<List style={{ marginBottom: 16 }}>
						<Card>
							<ListItem>
								<Left>

									<Body style={{ marginLeft: 0 }}>
										<Text note style={{ marginBottom: 8 }}>Status Warga</Text>
										<Form>
											<Item regular picker>
												<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
													style={{ width: undefined }}
													placeholder="Lorem Ipsum"
													placeholderStyle={{ color: "#bfc6ea" }}
													placeholderIconColor="#007aff"
													selectedValue={formktp.status_peserta}
													onValueChange={v => handleForm("status_peserta", v)}
												>
													<Picker.Item label="Transit" value="transit" />
													<Picker.Item label="Non Transit" value="non transit" />
												</Picker>
											</Item>
										</Form>
									</Body>
								</Left>
							</ListItem>
						</Card>
					</List>

					<Grid style={{ marginBottom: 16 }}>
						<Col>
							<Button block style={{ backgroundColor: "#FAB802", marginRight: 8 }}><Text>KTP</Text></Button>
						</Col>
						<Col>
							<Button block onPress={() => navigation.navigate("ProcessScreeningOneNonKtp")} style={{ borderColor: "#FAB802", borderWidth: 1, backgroundColor: "white", marginLeft: 8 }}><Text style={{ color: "#FAB802" }}>NON KTP</Text></Button>
						</Col>
					</Grid>

					<Grid style={{ marginBottom: 16 }}>
						<Col style={{ flex: 4 }}>
							<Item regular style={{ backgroundColor: "white" }}>
								<Input value={searchNoKtp} onChangeText={v => setSearchNoKtp(v)} placeholder="Masukkan No.KTP" />
							</Item>
						</Col>
						<Col style={{ flex: 2 }}>
							<Button block onPress={handleSearchKtp} style={{ backgroundColor: "#0586FE", marginLeft: 8 }}><Text>CARI</Text></Button>
						</Col>
					</Grid>

					<List>
						<Card>
							<CardItem header>
								<H3 style={{ color: "#FEAC0E" }}>Data Identitas</H3>
							</CardItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>No. KTP</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name='idcard' type="AntDesign" />
											<Input value={formktp.ktp_no} onChangeText={v => handleForm("ktp_no", v)} disabled={!formktp.isSearchKtp} placeholder="Tolong No. KTP Anda" />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>Nama</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon style={{ color: "#FEAC0E" }} name='user' type="AntDesign" />
											<Input value={formktp.ktp_nama} onChangeText={v => handleForm("ktp_nama", v)} disabled={!formktp.isSearchKtp} placeholder="Tolong input nama Anda" />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>Tempat Lahir</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name='location' type="Entypo" />
											<Input value={formktp.tempat_lahir} onChangeText={v => handleForm("tempat_lahir", v)} disabled={!formktp.isSearchKtp} placeholder="Tolong input tempat lahir Anda" />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>Tanggal Lahir</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name="calendar" type="SimpleLineIcons" />											
											<DatePicker
												defaultDate={moment(formktp.tgl_lahir, "YYYY-MM-DD").toDate()} 
												placeHolderText={!formktp.tgl_lahir ? "Select Date" : null}
												minimumDate={new Date(1945, 1, 1)}
												maximumDate={new Date(2020, 12, 12)}
												locale={"en"}
												timeZoneOffsetInMinutes={undefined}
												modalTransparent={false}
												animationType={"fade"}
												androidMode={"default"}
												textStyle={{ color: "black" }}
												placeHolderTextStyle={{ color: "#d3d3d3" }}
												onDateChange={v => handleDate("tgl_lahir", v)}
												disabled={!formktp.isSearchKtp} 
											/>
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note style={{ marginBottom: 8 }}>Negara</Text>
										<Form>
											<Item regular picker>
												<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
													style={{ width: undefined }}
													placeholder="Lorem Ipsum"
													placeholderStyle={{ color: "#bfc6ea" }}
													placeholderIconColor="#007aff"
													selectedValue={formktp.negara_id}
													onValueChange={v => handleForm("negara_id", v)}
													enabled={formktp.isSearchKtp}
												>
													<Picker.Item value="" label="-" />
													{negara.map(x => (
														<Picker.Item key={x.id} value={x.id} label={x.nama} />
													))}
												</Picker>
											</Item>
										</Form>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note style={{ marginBottom: 8 }}>Provinsi</Text>
										<Form>
											<Item regular picker>
												<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
													style={{ width: undefined }}
													placeholder="Lorem Ipsum"
													placeholderStyle={{ color: "#bfc6ea" }}
													placeholderIconColor="#007aff"
													selectedValue={formktp.prov_id}
													onValueChange={v => handleChangeProvinsi("prov_id", v)}
													enabled={formktp.isSearchKtp}
												>
													<Picker.Item value="" label="-" />
													{provinsi.map(x => (
														<Picker.Item key={x.id} value={x.id} label={ucfirst(x.nama)} />
													))}
												</Picker>
											</Item>
										</Form>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note style={{ marginBottom: 8 }}>Kota/Kabupaten</Text>
										<Form>
											<Item regular picker>
												<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
													style={{ width: undefined }}
													placeholder="Lorem Ipsum"
													placeholderStyle={{ color: "#bfc6ea" }}
													placeholderIconColor="#007aff"
													selectedValue={formktp.kab_id}
													onValueChange={v => handleChangeKabupaten("kab_id", v)}
													enabled={formktp.isSearchKtp}
												>
													<Picker.Item value="" label={kabupaten.length > 0 ? "-" : "Pilih Provinsi dahulu"} />
													{kabupaten.map(x => (
														<Picker.Item key={x.id} value={x.id} label={ucfirst(x.nama)} />
													))}
												</Picker>
											</Item>
										</Form>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note style={{ marginBottom: 8 }}>Kecamatan</Text>
										<Form>
											<Item regular picker>
												<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
													style={{ width: undefined }}
													placeholder="Lorem Ipsum"
													placeholderStyle={{ color: "#bfc6ea" }}
													placeholderIconColor="#007aff"
													selectedValue={formktp.kec_id}
													onValueChange={v => handleChangeKecamatan("kec_id", v)}
													enabled={formktp.isSearchKtp}
												>
													<Picker.Item value="" label={kecamatan.length > 0 ? "-" : "Pilih Kota/Kabupaten dahulu"} />
													{kecamatan.map(x => (
														<Picker.Item key={x.id} value={x.id} label={ucfirst(x.nama)} />
													))}
												</Picker>
											</Item>
										</Form>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note style={{ marginBottom: 8 }}>Desa/Kelurahan</Text>
										<Form>
											<Item regular picker>
												<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
													style={{ width: undefined }}
													placeholder="Lorem Ipsum"
													placeholderStyle={{ color: "#bfc6ea" }}
													placeholderIconColor="#007aff"
													selectedValue={formktp.kel_id}
													onValueChange={v => handleForm("kel_id", v)}
													enabled={formktp.isSearchKtp}
												>
													<Picker.Item value="" label={kelurahan.length > 0 ? "-" : "Pilih Kecamatan dahulu"} />
													{kelurahan.map(x => (
														<Picker.Item key={x.id} value={x.id} label={ucfirst(x.nama)} />
													))}
												</Picker>
											</Item>
										</Form>
									</Body>
								</Left>
							</ListItem>


							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>Dusun</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name='location-city' type="MaterialIcons" />
											<Input value={formktp.dusun} onChangeText={v => handleForm("dusun", v)} disabled={!formktp.isSearchKtp} placeholder="nama dusun" />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>RT/RW</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name='location-city' type="MaterialIcons" />
											<Input value={formktp.rtrw} onChangeText={v => handleForm("rtrw", v)} disabled={!formktp.isSearchKtp} placeholder="RT/RW" />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<CardItem header>
								<H3 style={{ color: "#FEAC0E" }}>Nomor Kontak Warga</H3>
							</CardItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>No Telp WA</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon style={{ color: "#FEAC0E" }} name='whatsapp' type="FontAwesome" />
											<Input keyboardType="phone-pad" value={formktp.wa_no} onChangeText={v => handleForm("wa_no", v)} onKeyPress={handleEnterNoHp} disabled={!formktp.isWa} placeholder="Input No. WA" />
										</Item>
									</Body>
								</Left>
								<Right>
									<Text />
									<Badge style={{backgroundColor: "#0677F9", marginBottom: 5 }}>
										<Text style={{fontSize: 12}}>
											{formktp.isWa ? "Ya" : "Tidak"}
										</Text>
									</Badge>
									<Switch style={{marginBottom: 4}} value={formktp.isWa} onValueChange={handleCheckWa} />
								</Right>
							</ListItem>
							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>No. Telp Pribadi (Voice Contact)</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon style={{ color: "#FEAC0E" }} name='whatsapp' type="FontAwesome" />
											<Input keyboardType="phone-pad" value={formktp.telp_no} onChangeText={v => handleForm("telp_no", v)} disabled={!formktp.isTelp} placeholder="Input No. Telp Pribadi" />
										</Item>
									</Body>
								</Left>
								<Right>
									<Text />
									<Badge style={{backgroundColor: "#0677F9", marginBottom: 5 }}>
										<Text style={{fontSize: 12}}>
											{formktp.isTelp ? "Ya" : "Tidak"}
										</Text>
									</Badge>
									<Switch value={formktp.isTelp} onValueChange={handleCheckTelp} style={{marginBottom: 4}} />
								</Right>
							</ListItem>						
						</Card>
					</List>

					<Button
						block
						style={{ backgroundColor: "#FAB802", marginTop: 16 }}
						onPress={handleFastScreening}>
						<Text>SCREENING CEPAT</Text>
					</Button>

					<Grid style={{ marginTop: 16 }}>
						<Col>
							<Button onPress={handleReset} block style={{ backgroundColor: "#DA132F", marginRight: 8 }}><Text>HAPUS</Text></Button>
						</Col>
						<Col>
							<Button onPress={handleNext} block style={{ backgroundColor: "#0677F9", marginLeft: 8 }}><Text>NEXT</Text></Button>
						</Col>
					</Grid>

				</Content>
			</ScrollView>
		</Container>
	)
}

export default ProcessScreeningOne
