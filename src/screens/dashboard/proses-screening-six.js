import React, {useState} from "react"
import { Body, Textarea, Grid, Badge, Col, Button, Card, CardItem, Container, Content, DatePicker, Form, H3, Header, Icon, Input, Item, Left, List, ListItem, Picker, Right, Text } from "native-base"
import { ScrollView } from "react-native-gesture-handler"
import { useSelector, useDispatch } from "react-redux"
import Loader from "../screening/Loader"
import styles from "./styles"
import moment from "moment"
import "moment/locale/id"
import { screening_handleFormLain, screening_handleFormRiwayat, screening_setRiwayat, screening_saveScreening } from "../../stores/screening.action"
import { Alert } from "react-native"

const ProcessScreeningSix = ({ navigation }) => {
	const { root: { user }, screening: { formlain, formriwayat, riwayat } } = useSelector(state => state)
	const dispatch = useDispatch()

	const [isDisable, setDisable] = useState(false)

	const handleFormLain = (name, value) => dispatch(screening_handleFormLain(name, value))
	const handleFormRiwayat = (name, value) => dispatch(screening_handleFormRiwayat(name, value))
	const handleDate = (name, value) => dispatch(screening_handleFormRiwayat(name, moment(value).format("YYYY-MM-DD")))

	const handleAddRiwayat = () => {
		if (
			formriwayat.tanggal !== "" &&
			formriwayat.rute !== "" &&
			formriwayat.durasi !== "" &&
			formriwayat.jenis_transportasi !== ""
		) {
			dispatch(screening_setRiwayat({ ...formriwayat }))
		} else {
			Alert.alert("Data tidak lengkap", "Silahkan lengkapi data riwayat untuk menambah riwayat.")
		}
	}

	const handleBack = () => {
		navigation.navigate("ProcessScreeningFive")
	}
	const handleSave = () => {
		setDisable(true)
		dispatch(screening_saveScreening(navigation)).then(() => {						
			setDisable(false)			
		})
	}

	return (
		<Container style={styles.container}>
			<Header transparent androidStatusBarColor="#fff">
				<Left>
					<Button transparent onPress={handleBack}>
						<Icon style={{ color: "#4F4F4F" }} name="md-arrow-back" />
					</Button>
				</Left>
				<Body>
					<Text style={{ color: "#4F4F4F" }}>Process Screening</Text>
				</Body>
				<Right />
			</Header>

			<ScrollView style={{ marginTop: 8 }} >
				<Content padder style={{ paddingBottom: 150 }}>

					<List>
						<Card>
							<CardItem header>
								<H3 style={{ color: "#FEAC0E", fontSize: 16 }}>Informasi Lain</H3>
							</CardItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text style={{ marginBottom: 8 }} note>Apakah ada tetangga yang POSITIF Covid19?</Text>
										<Form>
											<Item regular picker>
												<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
													style={{ width: undefined }}
													placeholder="Select your SIM"
													placeholderStyle={{ color: "#bfc6ea" }}
													placeholderIconColor="#007aff"
													selectedValue={formlain.tetangga_covid}
													onValueChange={v => handleFormLain("tetangga_covid", v)}
												>
													<Picker.Item label="Tidak" value="tidak" />
													<Picker.Item label="Iya" value="iya" />
												</Picker>
											</Item>
										</Form>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>

									<Body style={{ marginLeft: 0 }}>
										<Text style={{ marginBottom: 8 }} note>Catatan lain</Text>
										<Form>
											<Textarea value={formlain.catatan} onChangeText={v => handleFormLain("catatan", v)} rowSpan={5} bordered placeholder="Silahkan masukkan catatan Anda" />
										</Form>
									</Body>
								</Left>
							</ListItem>



							<CardItem header>
								<Text style={{ color: "#FEAC0E", fontSize: 16 }}>Riwayat Perjalanan/Persinggahan</Text>
							</CardItem>
							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>Hari & Tanggal</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name="calendar" type="SimpleLineIcons" />											
											<DatePicker
												defaultDate={new Date(2020, 1, 1)}
												minimumDate={new Date(1945, 1, 1)}
												maximumDate={new Date(2020, 12, 12)}
												locale={"en"}
												timeZoneOffsetInMinutes={undefined}
												modalTransparent={false}
												animationType={"fade"}
												androidMode={"default"}
												placeHolderText="Select date"
												textStyle={{ color: "black" }}
												placeHolderTextStyle={{ color: "#d3d3d3" }}
												onDateChange={v => handleDate("tanggal", v)}
												disabled={false} />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>Rute Perjalanan/Persinggahan</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name='search-location' type="FontAwesome5" />
											<Input value={formriwayat.rute} onChangeText={v => handleFormRiwayat("rute", v)} placeholder="Rute perjalanan" />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>Durasi Persinggahan</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name='car-side' type="FontAwesome5" />
											<Input value={formriwayat.durasi} onChangeText={v => handleFormRiwayat("durasi", v)} placeholder="Masukkan lama waktu singgah" />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Grid style={{ marginBottom: 16 }}>
											<Col style={{ flex: 4 }}>
												<Text style={{ marginBottom: 8 }} note>Kendaraan yang digunakan</Text>
												<Form>
													<Item regular picker>
														<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
															style={{ width: undefined }}
															placeholder="Select your SIM"
															placeholderStyle={{ color: "#bfc6ea" }}
															placeholderIconColor="#007aff"
															selectedValue={formriwayat.jenis_transportasi}
															onValueChange={v => handleFormRiwayat("jenis_transportasi", v)}
														>
															<Picker.Item label="Umum" value="umum" />
															<Picker.Item label="Sewa" value="sewa" />
															<Picker.Item label="Pribadi" value="pribadi" />
														</Picker>
													</Item>
												</Form>
											</Col>
											<Col style={{ flex: 2 }}>
												<Text style={{ marginBottom: 8 }} />
												<Button block
													onPress={handleAddRiwayat}
													style={{ backgroundColor: "#0586FE", marginLeft: 8 }}><Text>TAMBAH</Text></Button>
											</Col>
										</Grid>
									</Body>
								</Left>
							</ListItem>

						</Card>
					</List>

					<H3 style={{ marginTop: 16, color: "#FEAC0E", fontWeight: "normal", marginBottom: 8 }}>Daftar Riwayat Perjalanan</H3>
					<List>
						{riwayat.map((data, i) => (
							<Card key={data.riwayat_no}>
								<ListItem>
									<Left>
										<Body>
											<Text style={{ fontWeight: "bold", color: "#4F4F4F", fontSize: 10 }}>PERJALANAN KE {i + 1}</Text>
										</Body>
									</Left>
									<Body>
										<Text style={{ fontWeight: "bold", color: "#4F4F4F", fontSize: 10 }}>Tanggal</Text>
										<Text style={{ color: "#0586FE", fontSize: 10 }}>{moment(data.tanggal).format("dddd, DD MM YYYY")}</Text>
									</Body>
									<Right>
										<Badge danger>
											<Text style={{ fontSize: 12 }}>
												{/* {data.durasi} */}
												X
											</Text>
										</Badge>
									</Right>
								</ListItem>
								<ListItem>
									<Left>
										<Body>
											<Text style={{ fontSize: 12, fontWeight: "normal", color: "#4F4F4F" }}>Rute Perjalanan</Text>
											<Text style={{ fontWeight: "bold", color: "#4F4F4F" }}>{data.rute}</Text>
										</Body>
									</Left>
									<Right>
										<Text style={{ fontSize: 12, fontWeight: "normal", color: "#4F4F4F" }}>Kendaraan</Text>
										<Text style={{ fontWeight: "bold", color: "#4F4F4F" }}>{data.jenis_transportasi}</Text>
									</Right>
								</ListItem>
							</Card>
						))}
					</List>

					{isDisable ?
					<Loader show={true} />
					:
					<Button
						block
						disabled={isDisable}
						style={{ backgroundColor: "#0677F9", marginTop: 16 }}
						onPress={handleSave}>
						<Text>SIMPAN</Text>
					</Button>}

				</Content>
			</ScrollView>
		</Container>
	)
}

export default ProcessScreeningSix
