import React, { useState, useEffect } from "react"
import { Dimensions, Image, View, Alert, TouchableOpacity } from "react-native"
import { Button, Container, Content, Header, Icon, Input, Item, Text } from "native-base"
import { useSelector, useDispatch } from "react-redux"
import styles from "./styles"
import { get, post } from "../../helpers/fetch"

import { root_setUser, root_setLogged } from "../../stores/root.action"


const deviceWidth = Dimensions.get("window").width

const LoginUser = ({ navigation }) => {
	const { isLogged, user } = useSelector(state => state.root)
	const dispatch = useDispatch()
	const [form, setForm] = useState({
		username: "",
		password: "",
	})
	const [visible, setVisible] = useState(false)

	const handleForm = (name, value) => setForm({ ...form, [name]: value })
	const handleSubmit = () => {
		const { username, password } = form
		if (username && password) {
			post(`login`, form).then(res => {				
				if (res.status === "success") {
					dispatch(root_setUser(res.data))
					dispatch(root_setLogged(true))
					navigation.navigate("Intro")
				} else {
					Alert.alert("Data Tidak cocok", res.message)
				}
			})
			.catch(() => Alert.alert("Error","Tidak dapat terhubung ke server"))
		} else {
			Alert.alert("Lengkapi Data","Silakan isi nomor hp & password")
		}
	}

	const handleGet = () => {
		const { username, password } = form
		if (username && password) {
			get('user/3').then(res => {
				dispatch(root_setUser(res[0]))
				dispatch(root_setLogged(true))
				navigation.navigate("Intro")
			})
		} else {
			Alert.alert("Lengkapi Data","Silakan isi nomor hp & password")
		}
	}

	useEffect(() => {
		if (user && isLogged === true) {
			navigation.navigate("Intro")
		}
	}, [])

	return (
		<Container style={styles.container}>
			<Header transparent style={{ height: 0 }} androidStatusBarColor="#fff" />

			<Image source={require("../../assets/img/background-forgot-password.jpg")}
				style={{ height: 300, width: deviceWidth, flex: 1 }} />

			<View style={{ position: "absolute", left: 8, right: 8, top: 150 }}>
				<Content padder>
					<View style={{ justifyContent: "center", alignItems: "center" }}>
						<Image source={require("../../assets/img/logo-besar-2-Putih.png")} />
					</View>

					<Item rounded style={{ marginTop: 28, backgroundColor: "white" }}>
						<Icon style={{ color: "#FEAC0E" }} name="screen-smartphone" type="SimpleLineIcons" />
						<Input value={form.username} onChangeText={v => handleForm("username", v)} placeholder="No HP" />
					</Item>
					<Item rounded style={{ marginTop: 24, backgroundColor: "white" }}>
						<Icon style={{ color: "#FEAC0E" }} name="lock" type="SimpleLineIcons" />
						<Input value={form.password} onChangeText={v => handleForm("password", v)} secureTextEntry={!visible} placeholder="Password" />
						<Icon onPress={() => setVisible(!visible)} active name='eye' type="SimpleLineIcons" style={{ marginRight: 8 }} />
					</Item>
					
					<TouchableOpacity>
						<Text>Lupa password</Text>
					</TouchableOpacity>

					<TouchableOpacity
						style={{ marginTop: 24, backgroundColor: "#FEAC0E", paddingTop: 15, paddingBottom: 15, borderRadius: 50 }}
						onPress={handleSubmit}
					>
						<Text style={{ textAlign: 'center', color: 'white' }}>LOGIN</Text>
					</TouchableOpacity>

					<View style={{ justifyContent: "center", alignItems: "center", marginTop: 8 }}>
						<Text note>Ninecloud &copy; 2020</Text>
						<Text note onLongPress={handleGet}>V.1.0</Text>
					</View>

				</Content>
			</View>
		</Container>
	)
}

export default LoginUser
