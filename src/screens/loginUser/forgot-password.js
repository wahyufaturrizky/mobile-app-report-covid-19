import React from "react"
import { Dimensions, Image, View } from "react-native"
import { Body, Button, Container, Content, Header, Icon, Input, Item, Left, Right, Text } from "native-base"
import styles from "./styles"
import { get } from "../../helpers/fetch"

const deviceWidth = Dimensions.get("window").width


const ForgotPassword = ({ navigation }) => {

	const [form, setForm] = useState({
		username: "",
		email: "",
	})

	const handleForm = (name, value) => setForm({ ...form, [name]: value })
	const handleSubmit = () => {
		get(`forgot`, form).then(res => {
			if (res.success) {
				alert(res.success)
			} else if (res.failed) {
				alert(res.failed)
			} else {
				console.log(res)
			}
		})
	}

	return (
		<Container style={styles.container}>
			<Header transparent androidStatusBarColor="#fff">
				<Left>
					<Button onPress={() => navigation.navigate("LoginUser")} transparent>
						<Icon style={{ color: "#4F4F4F" }} name="md-arrow-back" />
					</Button>
				</Left>
				<Body>
					<Text style={{ color: "#4F4F4F" }}>Lupa Password</Text>
				</Body>
				<Right />
			</Header>

			<Image source={require("../../assets/img/background-forgot-password.jpg")}
				style={{ height: 300, width: deviceWidth, flex: 1 }} />

			<View style={{ position: "absolute", left: 8, right: 8, top: 250 }}>
				<Content padder>

					<Text style={{ marginBottom: -15 }} note>Silahkan masukkan no HP Anda yang terdaftar</Text>
					<Item rounded style={{ marginTop: 24, backgroundColor: "white" }}>
						<Icon style={{ color: "#FEAC0E" }} name="screen-smartphone" type="SimpleLineIcons" />
						<Input value={form.username} onChangeText={v => handleForm("username", v)} placeholder="No HP" />
					</Item>
					<Item rounded style={{ marginTop: 24, backgroundColor: "white" }}>
						<Icon style={{ color: "#FEAC0E" }} name="envelope" type="SimpleLineIcons" />
						<Input value={form.email} onChangeText={v => handleForm("email", v)} placeholder="E-mail" />
					</Item>

					<Button
						onPress={handleSubmit}
						block
						rounded
						style={{ marginTop: 24, backgroundColor: "#FEAC0E" }}
					>
						<Text>SEND</Text>
					</Button>

					<View style={{ justifyContent: "center", alignItems: "center", marginTop: 20 }}>
						<Text note>RSUD Sidoarjo &copy; 2020</Text>
						<Text note>V - 1.0</Text>
					</View>

				</Content>
			</View>
		</Container>
	)
}

export default ForgotPassword
