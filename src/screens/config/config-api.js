import React, { useState, useEffect } from "react"
import { List, ListItem, Item, Text, Content, Card, Left, Body, Icon, Input, Button } from "native-base"
import { ScrollView } from "react-native-gesture-handler"
import AsyncStorage from "@react-native-community/async-storage"
import { useSelector } from "react-redux"



const ConfigApi = ({ navigation }) => {
    const { user, isLogged } = useSelector(state => state.root)
    const [baseApi, setBaseApi] = useState("")

    const handleForm = value => setBaseApi(value)
    const handleSave = () => {
        AsyncStorage.setItem("@baseApi", baseApi, () => {
            if (user && isLogged === true) {
                navigation.navigate("Intro")
            } else {        
                navigation.navigate("LoginUser")
            }
        })
    }

    useEffect(() => {
        const getBaseApi = async () => {
            AsyncStorage.getItem("@baseApi", (err, res) => {
                setBaseApi(res)
            })
        }
        getBaseApi()
    }, [setBaseApi])

    return (
        <ScrollView style={{ marginTop: 20 }} >
            <Content padder>
                <List style={{ marginBottom: 16 }}>
                    <Card>
                        <ListItem>
                            <Left>
                                <Body style={{ marginLeft: 0 }}>
                                    <Text note>Base API</Text>
                                    <Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
                                        <Icon active style={{ color: "#FEAC0E" }} name='database' type="Entypo" />
                                        <Input value={baseApi} onChangeText={v => handleForm(v)} placeholder="ex: http://127.0.0.1:13131" />
                                    </Item>
                                </Body>
                            </Left>
                        </ListItem>
                    </Card>
                </List>

                <Button
                    block
                    style={{ backgroundColor: "#0677F9", marginTop: 16 }}
                    onPress={handleSave}>
                    <Text>Simpan</Text>
                </Button>

            </Content>
        </ScrollView>
    )
}

export default ConfigApi