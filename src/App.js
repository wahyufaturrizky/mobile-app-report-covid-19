import React from "react"
import { Root } from "native-base"
import { NavigationContainer } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import { Provider } from "react-redux"
import { PersistGate } from "redux-persist/integration/react"
import { store, persistor } from "./configStore"

// Menu Auth
import LoginUser from "./screens/loginUser/index"
import ForgotPassword from "./screens/loginUser/forgot-password"
import Intro from "./screens/loginUser/intro"

// Menu Profile
import MyProfile from "./screens/myProfile/index"
import MyProfileKeamanan from "./screens/myProfile/myprofile-kemanan"
import DetailProfil from "./screens/myProfile/detail-profil"
import EditProfil from "./screens/myProfile/edit-profil"
import UbahKataSandi from "./screens/myProfile/ubdah-kata-sandi"

// Menu Dashboard
import Dashboard from "./screens/dashboard/index"
import ProcessScreeningOne from "./screens/dashboard/proses-screening-one"
import ProcessScreeningTwo from "./screens/dashboard/proses-screening-two"
import ProcessScreeningThree from "./screens/dashboard/proses-screening-three"
import ProcessScreeningFour from "./screens/dashboard/proses-screening-four"
import ProcessScreeningFive from "./screens/dashboard/proses-screening-five"
import ProcessScreeningSix from "./screens/dashboard/proses-screening-six"
import ProcessScreeningSeven from "./screens/dashboard/proses-screening-seven"
import ProcessScreeningOneNonKtp from "./screens/dashboard/proses-screening-one-non-ktp"
import ProcessScreeningDataLokasiSaatIni from "./screens/dashboard/proses-screening-one-data-lokasi-saat-ini"
import ProcessScreeningCepat from "./screens/dashboard/proses-screening-screen-cepat"

// Screening
import ScreeningListView from "./screens/screening/ScreeningListView"
import DetailSparePart from "./screens/screening/sparepart-detail"
import ScreeningProcessScreeningOne from "./screens/screening/screen-proses-screening-one"
import ScreeningProcessScreeningTwo from "./screens/screening/screen-proses-screening-two"
import ScreeningProcessScreeningThree from "./screens/screening/screen-proses-screening-three"

import ConfigApi from "./screens/config/config-api"



const { Navigator, Screen } = createStackNavigator()

export default () =>
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <NavigationContainer>
                <Root>
                    <Navigator initialRouteName="LoginUser" screenOptions={{ headerShown: false }}>
                        <Screen name="ConfigApi" component={ConfigApi} />
                        <Screen name="LoginUser" component={LoginUser} />
                        <Screen name="ForgotPassword" component={ForgotPassword} />
                        <Screen name="Intro" component={Intro} />


                        <Screen name="Dashboard" component={Dashboard} />
                        <Screen name="ProcessScreeningOne" component={ProcessScreeningOne} />
                        <Screen name="ProcessScreeningOneNonKtp" component={ProcessScreeningOneNonKtp} />
                        <Screen name="ProcessScreeningDataLokasiSaatIni" component={ProcessScreeningDataLokasiSaatIni} />
                        <Screen name="ProcessScreeningTwo" component={ProcessScreeningTwo} />
                        <Screen name="ProcessScreeningThree" component={ProcessScreeningThree} />
                        <Screen name="ProcessScreeningFour" component={ProcessScreeningFour} />
                        <Screen name="ProcessScreeningFive" component={ProcessScreeningFive} />
                        <Screen name="ProcessScreeningSix" component={ProcessScreeningSix} />
                        <Screen name="ProcessScreeningSeven" component={ProcessScreeningSeven} />
                        <Screen name="ProcessScreeningCepat" component={ProcessScreeningCepat} />


                        <Screen name="ScreeningListView" component={ScreeningListView} />
                        <Screen name="ScreeningProcessScreeningOne" component={ScreeningProcessScreeningOne} />
                        <Screen name="ScreeningProcessScreeningTwo" component={ScreeningProcessScreeningTwo} />
                        <Screen name="ScreeningProcessScreeningThree" component={ScreeningProcessScreeningThree} />
                        <Screen name="DetailSparePart" component={DetailSparePart} />


                        <Screen name="MyProfile" component={MyProfile} />
                        <Screen name="MyProfileKeamanan" component={MyProfileKeamanan} />
                        <Screen name="EditProfil" component={EditProfil} />
                        <Screen name="DetailProfil" component={DetailProfil} />
                        <Screen name="UbahKataSandi" component={UbahKataSandi} />
                        
                    </Navigator>
                </Root>
            </NavigationContainer>
        </PersistGate>
    </Provider>
