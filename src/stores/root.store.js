const initialState = {
    isLogged: false,
    user: null,
    negara: [],
    provinsi: [],
    kabupaten: [],
    kecamatan: [],
    kelurahan: [],    
}

const root = (state = initialState, action) => {
    const { type, payload } = action
    switch(type) {
        case "SET_LOGGED":
            return {
                ...state,
                isLogged: payload
            }
        case "SET_USER":
            return {
                ...state,
                user: { ...state.user, ...payload }
            }
        case "SET_NEGARA":
            return {
                ...state,
                negara: payload
            }
        case "SET_PROVINSI":
            return {
                ...state,
                provinsi: payload
            }
        case "SET_KABUPATEN":
            return {
                ...state,
                kabupaten: payload
            }
        case "SET_KECAMATAN":
            return {
                ...state,
                kecamatan: payload
            }
        case "SET_KELURAHAN":
            return {
                ...state,
                kelurahan: payload
            }
        default:
            return state

    }
}

export default root