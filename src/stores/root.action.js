
export const root_setLogged = payload => ({ type: "SET_LOGGED", payload })
export const root_setUser = payload => ({ type: "SET_USER", payload })

export const root_setNegara = payload => ({ type: "SET_NEGARA", payload })
export const root_setProvinsi = payload => ({ type: "SET_PROVINSI", payload })
export const root_setKabupaten = payload => ({ type: "SET_KABUPATEN", payload })
export const root_setKecamatan = payload => ({ type: "SET_KECAMATAN", payload })
export const root_setKelurahan = payload => ({ type: "SET_KELURAHAN", payload })